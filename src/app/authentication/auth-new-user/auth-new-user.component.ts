import {Component} from '@angular/core';
import {ViewChild} from '@angular/core';
import {NewUsernameComponent} from './new-username.component';
import {NewPasswordComponent} from './new-password.component';
import {NewFirstnameComponent} from './new-firstname.component';
import {NewLastnameComponent} from './new-lastname.component';
import {NewEmailComponent} from './new-email.component';
import {User} from './../../model/user';
import {AuthenticationService} from '../authentication.service';

@Component({
	selector: 'auth-new-user',
	template: `
		<new-username></new-username>
		<new-password></new-password>
		<new-firstname></new-firstname>
		<new-lastname></new-lastname>
		<new-email></new-email>
		<new-user-button (submittedNewUser)="submitUser()"></new-user-button>
	`
})

export class AuthNewUserComponent{
	@ViewChild(NewUsernameComponent) username: NewUsernameComponent;
	@ViewChild(NewPasswordComponent) password: NewPasswordComponent;
	@ViewChild(NewFirstnameComponent) firstname: NewFirstnameComponent;
	@ViewChild(NewLastnameComponent) lastname: NewLastnameComponent;
	@ViewChild(NewEmailComponent) email: NewEmailComponent;
	
	private authenticationService: AuthenticationService;

	constructor(authenticationService: AuthenticationService){
		this.authenticationService = authenticationService;	
	}

	private getNewUser(): User{
		return new User(
			this.username.getUsername(),
			this.password.getPassword(),
			this.firstname.getFirstname(),
			this.lastname.getLastname(),
			this.email.getEmail()
		);
	}

	private submitUser(): void{
		let user = this.getNewUser();
		console.log(user);
		this.authenticationService.saveNewUser(user);
	}
}
