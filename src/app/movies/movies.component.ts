import {Component} from '@angular/core';
import {MovieListComponent} from './movie-list.component';

@Component({
	selector: 'movies',
	template: `
		<div class="row">
			<div class="col-12">
				<h2>Movies</h2>
			</div>
		</div>
		<movie-list></movie-list>
	`
})

export class MoviesComponent{

}
