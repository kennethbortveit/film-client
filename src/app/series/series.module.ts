import {NgModule} from '@angular/core';
import {SerieListComponent} from './serie-list.component';
import {BrowserModule} from '@angular/platform-browser';
import {SerieComponent} from './serie.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
	declarations: [
		SerieListComponent,
		SerieComponent
	],
	imports: [
		BrowserModule,
		NgbModule
	],
	exports: [
		SerieListComponent,
		SerieComponent
	]
})

export class SeriesModule{

}
