import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Headers} from '@angular/http';
import {User} from '../model/user';

@Injectable()

export class AuthenticationService{
	
	private http: Http;

	constructor(http: Http){
		this.http = http;
	}

	public login(username: string, password: string){
		let url = 'https://film-server-demo.herokuapp.com/login';
		let body = 
			'username=' + username +
			'&password=' + password;
		let headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		this.http.post(url, body, {headers: headers})
		.map((response: Response) => {
			return response.json();	
		})
		.subscribe((mappedResponse: any) => {
			localStorage.setItem('token', mappedResponse.token);	
			console.log(mappedResponse.msg);
		});
	}

	public logout(): void{
		localStorage.clear();
	}

	public getLoginState(): boolean{
		return localStorage.getItem('token') != null;	
	}

	public saveNewUser(user: User): void{
		let url = 'http://film-server-demo.herokuapp.com/new-user';
		let body = 
			'username=' + user.getUsername() +
			'&password=' + user.getPassword() +
			'&firstname=' + user.getFirstname() +
			'&lastname=' + user.getLastname() +
			'&email=' + user.getEmail();
		let headers = new Headers();
		headers.append('Content-Type', 'application/x-www-form-urlencoded');
		this.http.post(url, body, {headers: headers})
		.subscribe((response: Response) => {
			console.log(response);	
		});
	}
}
