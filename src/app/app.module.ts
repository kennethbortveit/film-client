import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {MaterialModule} from '@angular/material';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {Routes} from '@angular/router';
import {AuthenticationModule} from './authentication/authentication.module';
import {MoviesModule} from './movies/movies.module';
import {SeriesModule} from './series/series.module';

import 'hammerjs';

import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MoviesComponent} from './movies/movies.component';
import {SeriesComponent} from './series/series.component';
import {AuthenticationComponent} from './authentication/authentication.component';

import {AuthenticationService} from './authentication/authentication.service';

const routes: Routes = [
	{path: '', redirectTo: '/authentication', pathMatch: 'full'},
	{path: 'movies', component: MoviesComponent},
	{path: 'series', component: SeriesComponent},
	{path: 'authentication', component: AuthenticationComponent}
];

@NgModule({
	declarations: [
		AppComponent,
		HeaderComponent,
		FooterComponent,
		MoviesComponent,
		SeriesComponent,
		AuthenticationComponent,
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		MaterialModule.forRoot(),
		NgbModule.forRoot(),
		RouterModule.forRoot(routes, {useHash: true}),
		AuthenticationModule,
		MoviesModule,
		SeriesModule
	],
	providers: [
		AuthenticationService	
	],
	bootstrap: [
		AppComponent
	]
})
export class AppModule {

}
