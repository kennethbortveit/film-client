import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {MaterialModule} from '@angular/material';
import {MovieListComponent} from './movie-list.component';
import {MovieComponent} from './movie.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
	declarations: [
		MovieListComponent,
		MovieComponent
	],
	imports: [
		BrowserModule,
		MaterialModule,
		NgbModule
	],
	exports: [
		MovieListComponent,
		MovieComponent
	]
})

export class MoviesModule{

}
