export class User{
	private username: string;
	private password: string;
	private firstname: string;
	private lastname: string;
	private email: string;

	constructor(
		username: string,
		password: string,
		firstname: string,
		lastname: string,
		email: string
	){
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
	}

	public getUsername(): string{
		return this.username;
	}
	public getPassword(): string{
		return this.password;
	}
	public getFirstname(): string{
		return this.firstname;
	}
	public getLastname(): string{
		return this.lastname;
	}
	public getEmail(): string{
		return this.email;
	}
}
