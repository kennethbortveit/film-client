import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Movie} from '../model/movie';

@Injectable()

export class MoviesService{
	
	private http: Http;

	constructor(http: Http){
		this.http = http;
	}
	
	public getMovies(): Observable<Movie[]> {
		return this.http.get('https://film-server-demo.herokuapp.com/movies?token=' + localStorage.getItem('token'))
		.map((response: Response) => {
			let movies: Movie[] = [];
			for(let movie of response.json()){
				movies.push(new Movie(movie.title, movie.videoUrl, movie.imageUrl));
			}
			return movies;
		});
	}

}
