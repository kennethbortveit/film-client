import {Component} from '@angular/core';
import {AuthenticationService} from '../authentication.service';
import {ViewChild} from '@angular/core';
import {AuthUsernameComponent} from './auth-username.component';
import {AuthPasswordComponent} from './auth-password.component';


@Component({
	selector: 'auth-login',
	template: `
		<auth-username></auth-username>
		<auth-password></auth-password>
		<auth-login-button (loginClicked)="login()"></auth-login-button>
	`
})

export class AuthLoginComponent{
	@ViewChild(AuthUsernameComponent) username: AuthUsernameComponent;
	@ViewChild(AuthPasswordComponent) password: AuthPasswordComponent;

	private authenticationService: AuthenticationService;

	constructor(authenticationService: AuthenticationService){
		this.authenticationService = authenticationService;
	}
	
	private login(): void{
		this.authenticationService.login(this.username.getUsername(), this.password.getPassword());
	}
}
