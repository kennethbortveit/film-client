export class Movie{

	private title: string;
	private videoUrl: string;
	private imageUrl: string;

	constructor(title: string, videoUrl: string, imageUrl: string){
		this.title = title;
		this.videoUrl = videoUrl;
		this.imageUrl = imageUrl;
	}

	public getTitle(): string{
		return this.title;
	}
	public getVideoUrl(): string{
		return this.videoUrl;
	}
	public getImageUrl(): string{
		return this.imageUrl;
	}
}
