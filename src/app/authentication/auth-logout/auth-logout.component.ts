import {Component} from '@angular/core';

@Component({
	selector: 'auth-logout',
	template: `
		<div class="row form-group mt-4">
			<div class="col-12">
				<button md-button (click)="logout()">Logg ut</button>
			</div>
		</div>
	`
})

export class AuthLogoutComponent{
	
	private logout(): void{
		localStorage.clear();
	}
}
