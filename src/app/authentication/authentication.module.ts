import {NgModule} from '@angular/core';
import {AuthNewUserModule} from './auth-new-user/auth-new-user.module';
import {AuthLoginModule} from './auth-login/auth-login.module';
import {MaterialModule} from '@angular/material';

import {AuthLoginComponent} from './auth-login/auth-login.component';
import {AuthLogoutComponent} from './auth-logout/auth-logout.component';
import {AuthNewUserComponent} from './auth-new-user/auth-new-user.component';

@NgModule({
	
	declarations: [
		AuthLoginComponent,
		AuthLogoutComponent,
		AuthNewUserComponent

	],

	imports: [
		AuthLoginModule,
		AuthNewUserModule,
		MaterialModule
	],

	exports: [
		AuthLoginComponent,
		AuthLogoutComponent,
		AuthNewUserComponent
	]
})

export class AuthenticationModule{

}
