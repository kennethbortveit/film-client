import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Serie} from '../model/serie';

@Injectable()

export class SeriesService{
	
	private http: Http;

	constructor(http: Http){
		this.http = http;
	}
	
	public getSeries(): Observable<Serie[]>{
		return this.http.get('https://film-server-demo.herokuapp.com/series?token=' + localStorage.getItem('token'))
		.map((response: Response) => {
			let series: Serie[] = [];
			for(let serie of response.json()){
				series.push(new Serie(serie.title, serie.episode, serie.videoUrl, serie.imageUrl));
			}
			return series;
		});
	}
}
