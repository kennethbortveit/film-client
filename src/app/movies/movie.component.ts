import {Component} from '@angular/core';
import {Input} from '@angular/core';
import {Movie} from '../model/movie';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
	selector: 'movie',
	template: `
		<div class="row">
			<div class="col-12">
				<div class="card-deck">
					<div class="card">
						<div class="card-header">
							<h3>{{movie.getTitle()}}</h3>
						</div>
						<div class="card-block">
						</div>
					</div>
					<div class="card">
						<div class="card-block">
							<img src="{{movie.getImageUrl()}}" class="img-fluid" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card mt-4">
					<div class="card-block">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe [src]="sanitizer.bypassSecurityTrustResourceUrl(movie.getVideoUrl())" class="embed-responsive-item" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	`
})

export class MovieComponent{
	@Input() private movie: Movie;	
	private sanitizer: DomSanitizer;

	constructor(sanitizer: DomSanitizer){
		this.sanitizer = sanitizer;
	}
}
