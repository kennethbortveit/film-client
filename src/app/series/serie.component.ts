import {Component} from '@angular/core';
import {Serie} from '../model/serie';
import {Input} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
	selector: 'serie',
	styles: [`
	`],
	template: `
		<div class="row">
			<div class="col-12">
				<div class="card-deck">
					<div class="card">
						<div class="card-header">
							<h3>{{serie.getTitle()}}</h3>
						</div>
						<div class="card-block">
							<div class="card-text">
								<p>Episode: {{serie.getEpisode()}}</p>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-block">
							<img src="{{serie.getImageUrl()}}" class="img-fluid" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col-12">
				<div class="card">
					<div class="card-block">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe [src]="sanitizer.bypassSecurityTrustResourceUrl(serie.getVideoUrl())" class="embed-responsive-item" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	`
})

export class SerieComponent{
	@Input() private serie: Serie;	
	private sanitizer: DomSanitizer;

	constructor(sanitizer: DomSanitizer){
		this.sanitizer = sanitizer;	
	}
}
