import {Component} from '@angular/core';

@Component({
	selector: 'auth-password',
	template: `
		<div class="row form-group">
			<label for="auth-password" class="col-3">Passord:</label>
			<div class="col-9">
				<input type="password" name="auth-password" class="form-control" [(ngModel)]="authPassword" />
			</div>
		</div>
	`
})

export class AuthPasswordComponent{
	
	private authPassword: string;

	constructor(){

	}

	public getPassword(): string{
		return this.authPassword;
	}
}
