import {Component} from '@angular/core';
import {AuthenticationService} from './authentication/authentication.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})

export class AppComponent {

	private authenticationService: AuthenticationService;

	constructor(authenticationService: AuthenticationService){
		this.authenticationService = authenticationService;
	}

	private getLoginState(): boolean{
		return this.authenticationService.getLoginState();
	}
}
