import {Component} from '@angular/core';

@Component({
	selector: 'series',
	template: `
		<div class="row">
			<div class="col-12">
				<h2>Series</h2>
			</div>
		</div>
		<serie-list></serie-list>
	`
})

export class SeriesComponent{

}
