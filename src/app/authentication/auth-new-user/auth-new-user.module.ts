import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '@angular/material';
import {NewUsernameComponent} from './new-username.component';
import {NewPasswordComponent} from './new-password.component';
import {NewFirstnameComponent} from './new-firstname.component';
import {NewLastnameComponent} from './new-lastname.component';
import {NewEmailComponent} from './new-email.component';
import {NewUserButtonComponent} from './new-user-button.component';

@NgModule({
	declarations: [
		NewUsernameComponent,
		NewPasswordComponent,
		NewFirstnameComponent,
		NewLastnameComponent,
		NewEmailComponent,
		NewUserButtonComponent
	],
	imports: [
		FormsModule,
		MaterialModule
	],
	exports: [
		NewUsernameComponent,
		NewPasswordComponent,
		NewFirstnameComponent,
		NewLastnameComponent,
		NewEmailComponent,
		NewUserButtonComponent
	]
})

export class AuthNewUserModule{

}
