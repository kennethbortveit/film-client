import {Component} from '@angular/core';

@Component({
	selector: 'new-firstname',
	template: `
		<div class="row form-group">
			<label for="new-firstname" class="col-3">Fornavn:</label>
			<div class="col-9">
				<input type="text" name="new-firstname" class="form-control" [(ngModel)]="newFirstname" />
			</div>
		</div>
	`
})

export class NewFirstnameComponent{
	private newFirstname: string;

	public getFirstname(): string{
		return this.newFirstname;
	}
}
