import {Component} from '@angular/core';
import {Output} from '@angular/core';
import {EventEmitter} from '@angular/core';

@Component({
	selector: 'auth-login-button',
	template: `
		<div class="row form-group">
			<div class="col-12">
				<button md-button (click)="login()">Logg inn</button>
			</div>
		</div>
	`
})

export class AuthLoginButtonComponent{
	
	@Output() private loginClicked: EventEmitter<boolean>;

	constructor(){
		this.loginClicked = new EventEmitter<boolean>();
	}
	
	private login(): void{
		this.loginClicked.emit();
	}

}
