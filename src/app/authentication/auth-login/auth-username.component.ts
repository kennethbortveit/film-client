import {Component} from '@angular/core';

@Component({
	selector: 'auth-username',
	template: `
		<div class="row form-group mt-4">
			<label for="auth-username" class="col-3">Brukernavn:</label>
			<div class="col-9">
				<input type="text" name="auth-username" class="form-control" [(ngModel)]="authUsername" />
			</div>
		</div>
	`
})

export class AuthUsernameComponent{
	private authUsername: string;

	constructor(){
		
	}

	public getUsername(): string{
		return this.authUsername;
	}
}
