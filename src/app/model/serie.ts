export class Serie{
	private title: string;
	private episode: string;
	private videoUrl: string;
	private imageUrl: string;

	constructor(title: string, episode: string, videoUrl: string, imageUrl: string){
		this.title = title;
		this.episode = episode;
		this.videoUrl = videoUrl;
		this.imageUrl = imageUrl;
	}

	public getTitle(): string{
		return this.title;
	}
	public getEpisode(): string{
		return this.episode;
	}
	public getVideoUrl(): string{
		return this.videoUrl;
	}
	public getImageUrl(): string{
		return this.imageUrl;
	}
}
