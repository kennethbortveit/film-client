import {Component} from '@angular/core';
import {Output} from '@angular/core';
import {EventEmitter} from '@angular/core';

@Component({
	selector: 'header',
	template: `
	<div class="row">
		<div class="col-2 offset-10">
			<button md-button (click)="openSidenav()">Meny</button>
		</div>
	</div>
	`
})

export class HeaderComponent{

	@Output() private menuButtonClicked: EventEmitter<boolean>;

	constructor(){
		this.menuButtonClicked = new EventEmitter<boolean>();
	}
	
	private openSidenav(): void{
		this.menuButtonClicked.emit();	
	}
}
