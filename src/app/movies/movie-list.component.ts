import {Component} from '@angular/core';
import {MoviesService} from './movies.service';
import {Movie} from '../model/movie';
import {OnInit} from '@angular/core';

@Component({
	selector: 'movie-list',
	template: `
		<ngb-accordion [closeOthers]="true">
			<ngb-panel *ngFor="let movie of movies" title="{{movie.getTitle()}}">
				<template ngbPanelContent>
					<movie [movie]="movie"></movie>
				</template>
			</ngb-panel>
		</ngb-accordion>
	`,
	providers: [
		MoviesService
	]
})

export class MovieListComponent implements OnInit{
	
	private movies: Movie[];
	private moviesService: MoviesService;
	
	constructor(moviesService: MoviesService){
		this.moviesService = moviesService;
	}

	ngOnInit(): void{
		this.getMovies();
	}
	
	private getMovies(): void{
		this.moviesService.getMovies()
		.subscribe((movies: Movie[]) => {
			this.movies = movies;
		});
	}

}
