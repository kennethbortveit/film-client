import {Component} from '@angular/core';
import {Output} from '@angular/core';
import {EventEmitter} from '@angular/core';

@Component({
	selector: 'new-user-button',
	template: `
		<div class="row form-group">
			<div class="col-9 offset-3">
				<button md-button (click)="submitUser()">Lag ny bruker</button>
			</div>
		</div>
	`
})

export class NewUserButtonComponent{

	@Output() private submittedNewUser: EventEmitter<boolean>;

	constructor(){
		this.submittedNewUser = new EventEmitter<boolean>();
	}
	
	private submitUser(): void{
		this.submittedNewUser.emit();
	}
}
