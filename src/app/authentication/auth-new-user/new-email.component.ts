import {Component} from '@angular/core';

@Component({
	selector: 'new-email',
	template: `
		<div class="row form-group">
			<label for="new-email" class="col-3">Epost:</label>
			<div class="col-9">
				<input type="text" name="new-email" class="form-control" [(ngModel)]="newEmail" />
			</div>
		</div>
	`
})

export class NewEmailComponent{
	private newEmail: string;

	public getEmail(): string{
		return this.newEmail;
	}
}
