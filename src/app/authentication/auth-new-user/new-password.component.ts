import {Component} from '@angular/core';

@Component({
	selector: 'new-password',
	template: `
		<div class="row form-group">
			<label for="newPassword" class="col-3">Passord:</label>
			<div class="col-9">
				<input type="password" name="new-password" class="form-control" [(ngModel)]="newPassword" />
			</div>
		</div>
	`
})

export class NewPasswordComponent{
	private newPassword: string;

	public getPassword(): string{
		return this.newPassword;
	}
}
