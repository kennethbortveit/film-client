import {Component} from '@angular/core';

@Component({
	selector: 'new-lastname',
	template: `
		<div class="row form-group">
			<label for="new-lastname" class="col-3">Etternavn:</label>
			<div class="col-9">
				<input type="text" name="new-lastname" class="form-control" [(ngModel)]="newLastname" />
			</div>
		</div>
	`
})

export class NewLastnameComponent{
	private newLastname: string;

	public getLastname(): string{
		return this.newLastname;
	}
}
