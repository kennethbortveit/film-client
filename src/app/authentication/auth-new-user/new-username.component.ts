import {Component} from '@angular/core';

@Component({
	selector: 'new-username',
	template: `
		<div class="row form-group mt-4">
			<label for="new-username" class="col-3">Brukernavn:</label>
			<div class="col-9">
				<input type="text" name="new-username" class="form-control" [(ngModel)]="newUsername" />
			</div>
		</div>
	`
})

export class NewUsernameComponent{
	private newUsername: string;
	
	public getUsername(): string{
		return this.newUsername;
	}
}
