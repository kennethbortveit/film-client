import {Component} from '@angular/core';
import {AuthenticationService} from './authentication.service';

@Component({
	selector: 'authentication',
	template: `
		<div class="row">
			<div class="col-12">
				<h2>Autentisering</h2>
			</div>
		</div>
		<md-tab-group>
			<md-tab label="Logg inn" *ngIf="!getLoginState()">
				<auth-login></auth-login>	
			</md-tab>
			<md-tab label="Logg ut" *ngIf="getLoginState()">
				<auth-logout></auth-logout>
			</md-tab>
			<md-tab label="Ny bruker">
				<auth-new-user></auth-new-user>
			</md-tab>
		</md-tab-group>
	`
})

export class AuthenticationComponent{
		
	private authenticationService: AuthenticationService;
	
	constructor(authenticationService: AuthenticationService){
		this.authenticationService = authenticationService;		
	}

	private getLoginState(): boolean{
		return this.authenticationService.getLoginState();
	}
}
