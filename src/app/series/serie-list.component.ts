import {Component} from '@angular/core';
import {Serie} from '../model/serie';
import {SeriesService} from './series.service';
import {OnInit} from '@angular/core';

@Component({
	selector: 'serie-list',
	template: `
		<ngb-accordion [closeOthers]="true">
			<ngb-panel *ngFor="let serie of series" title="{{serie.getTitle() + ': ' + serie.getEpisode()}}">
				<template ngbPanelContent>
					<serie [serie]="serie"></serie>
				</template>
			</ngb-panel>
		</ngb-accordion>
	`,
	providers: [
		SeriesService
	]
})

export class SerieListComponent implements OnInit{
	
	private series: Serie[];
	private seriesService: SeriesService;

	constructor(seriesService: SeriesService){
		this.seriesService = seriesService;
	}

	ngOnInit(): void {
		this.getSeries();
	}

	private getSeries(): void{
		this.seriesService.getSeries()
		.subscribe((series: Serie[]) => {
			this.series = series;
		});
	}
}
