import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from '@angular/material';
import {AuthUsernameComponent} from './auth-username.component';
import {AuthPasswordComponent} from './auth-password.component';
import {AuthLoginButtonComponent} from './auth-login-button.component';

@NgModule({
	declarations: [
		AuthUsernameComponent,
		AuthPasswordComponent,
		AuthLoginButtonComponent
	],
	imports: [
		FormsModule,
		MaterialModule
	],
	exports: [
		AuthUsernameComponent,
		AuthPasswordComponent,
		AuthLoginButtonComponent
	]
})

export class AuthLoginModule{

}
